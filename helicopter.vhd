---------------------------------------------------------------------------
---------------------------------------------------------------------------
--    Ball.vhd                                                           --
--    Viral Mehta                                                        --
--    Spring 2005                                                        --
--                                                                       --
--    Modified by Stephen Kempf 03-01-2006                               --
--                              03-12-2007                               --
--    Spring 2013 Distribution                                             --
--                                                                       --
--    For use with ECE 385 Lab 9                                         --
--    UIUC ECE Department                
--  



--   MODIFIED BALL FOR USE ON STUNTCOPTER
--   REMEMBER TO CHANGE BALL_SIZE to COPTER SIZE
                                --
---------------------------------------------------------------------------
---------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity helicopter is
   Port ( Reset : in std_logic;
        frame_clk : in std_logic;
		lrd : in std_logic_vector(2 downto 0); -- left right drop, corresponding to A, D, and SPACE
        BallX : out std_logic_vector(9 downto 0); --X coordinate of helicopter
        BallY : out std_logic_vector(9 downto 0); --Y coordinate of helicopter
		up, lft0rgt1, down : out std_logic); --testing using LEDs
end helicopter;

architecture Behavioral of helicopter is

signal Ball_X_pos, Ball_X_motion, Ball_Y_pos, Ball_Y_motion : std_logic_vector(9 downto 0);

constant Ball_X_Center : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(320, 10);  --Center position on the X axis
constant Ball_Y_Center : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(120, 10);  --Center position on the Y axis

constant Ball_X_Min    : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(0, 10);  --Leftmost point on the X axis
constant Ball_X_Max    : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(639, 10);  --Rightmost point on the X axis
constant Ball_Y_Min    : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(0, 10);   --Topmost point on the Y axis
constant Ball_Y_Max    : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(479, 10);  --Bottommost point on the Y axis
                              
constant Ball_X_Step   : std_logic_vector(9 downto 0) := "0000000011";  --Step size on the X axis
constant Ball_Y_Step   : std_logic_vector(9 downto 0) := "0000000010";  --Step size on the Y axis

begin

-------------------------------------------------

  Move_Ball: process(Reset, frame_clk, lrd)
  begin
    if(Reset = '1') then   --Asynchronous Reset
      Ball_Y_Motion <= "0000000000";
      Ball_X_Motion <= "0000000000";
      Ball_Y_Pos <= Ball_Y_Center;
      Ball_X_pos <= Ball_X_Center;

    elsif(rising_edge(frame_clk)) then
      if(Ball_X_Pos + "0000010110" >= Ball_X_Max) then -- Ball is at the right edge 
		if(lrd="010") then  -- prevents user from going out of boundary
			Ball_X_Motion <= "0000000000";  
			lft0rgt1 <= '1'; 
		elsif(lrd="100") then 
			Ball_X_Motion <= not(Ball_X_Step) + '1';
			lft0rgt1 <= '0';
			Ball_Y_pos <= Ball_Y_pos + Ball_Y_Motion;
			Ball_X_pos <= Ball_X_pos + Ball_X_Motion;
		end if;
		Ball_Y_Motion <= "0000000000"; -- we never want the helicopter to move up or down
		up <= '0';
		down <= '0';
      elsif(Ball_X_Pos - "0000001000" <= Ball_X_Min) then  -- Ball is at the left edge 
		if(lrd="100") then --prevents user from going out of boundary
			Ball_X_Motion <= "0000000000";
			lft0rgt1 <= '0';
		elsif(lrd="010") then--allows user to go right after hitting the boundary
			Ball_X_Motion <= Ball_X_Step; 
			lft0rgt1 <= '1';
			Ball_Y_pos <= Ball_Y_pos + Ball_Y_Motion;
			Ball_X_pos <= Ball_X_pos + Ball_X_Motion;
		end if;
		Ball_Y_Motion <= "0000000000"; --NO up or down
		up <= '0';
		down <= '0';
      elsif(lrd = "100") then --A move helicopter left
        Ball_X_Motion <= not(Ball_X_Step) + '1';
        Ball_Y_Motion <= "0000000000";
		lft0rgt1 <= '0';
		up <= '0';
		down <= '0';
	    Ball_Y_pos <= Ball_Y_pos + Ball_Y_Motion;
		Ball_X_pos <= Ball_X_pos + Ball_X_Motion;
      elsif(lrd = "010") then --D move helicopter right
        Ball_X_Motion <= Ball_X_Step;
        Ball_Y_Motion <= "0000000000"; 
		lft0rgt1 <= '1';
		up <= '0';
		down <= '0';
	    Ball_Y_pos <= Ball_Y_pos + Ball_Y_Motion;
		Ball_X_pos <= Ball_X_pos + Ball_X_Motion;
      else
      end if;
       
    end if;
  end process Move_Ball;

  BallX <= Ball_X_Pos; -- set outputs
  BallY <= Ball_Y_Pos;
 
end Behavioral;      
