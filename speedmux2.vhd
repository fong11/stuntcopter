--speed mux chooses speed of cart from a select input

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity speedmux2 is
	port( Sel : in std_logic_vector(2 downto 0);
		  Speed : out std_logic_vector(2 downto 0));
end speedmux2;

architecture behavioral of speedmux2 is
begin

with Sel select
	
	Speed <= "100" when "000",	--choose speed of 4 pixels/period when select is 0
			 "010" when "001",	--choose speed of 2 pixels/period when select is 1
			 "011" when "010",	--choose speed of 3 pixels/period when select is 2
			 "100" when "011",	--choose speed of 4 pixels/period when select is 3
			 "010" when "100",	--choose speed of 2 pixels/period when select is 4
			 "011" when "101",	--choose speed of 3 pixels/period when select is 5
			 "001" when "110",	--choose speed of 1 pixels/period when select is 6
			 "010" when "111",	--choose speed of 2 pixels/period when select is 7
			 "010" when others;	--choose speed of 2 pixels/period when select is anything else

end behavioral;
