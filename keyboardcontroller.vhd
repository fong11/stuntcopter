library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity keyboardcontroller is
   Port ( Clk : in std_logic;
		  reset : in std_logic;
          keycode : in std_logic_vector(7 downto 0);	-- receives keyboard make code
		  lrd : out std_logic_vector(2 downto 0); --left right down(space): only one is displayed at a time, indicating whether left, right, or down (space) has been pressed
		  enable : in std_logic;	--enables/disables the lrd output
		  enter : out std_logic);	--output indicating whether enter has been pressed
end entity;

architecture Behavioral of keyboardcontroller is

begin

cool:process(Clk,reset)is

begin
  if (enable = '1') then
	if (reset='1') then
		lrd <= "000";
		enter <= '0';
	elsif(rising_edge(Clk)) then
		if (keycode = x"1C") then	--left has been pressed
			lrd <= "100";
			enter <= '0';
		elsif(keycode = x"23") then	--right has been pressed
			lrd <= "010";
			enter <= '0';
		elsif(keycode = x"29") then	--space has been pressed
			lrd <= "001";
			enter <= '0';
		else	--nothing pressed
			lrd <= "000";
			enter <= '0';
		end if;
	end if;
  elsif (keycode = x"5A") then	--enter has been pressed
	lrd <= "000";
	enter <= '1';
  else	--nothing pressed
	lrd <= "000";
	enter <= '0';
  end if;
end process;

end Behavioral;