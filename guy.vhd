library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--This entity represents the guy
entity guy is
   Port ( Reset : in std_logic;
        frame_clk : in std_logic;
		lrd : in std_logic_vector(2 downto 0); -- left right drop. corresponding to A , D, and SPACE on the keyboard
		detect : in std_logic; --if the collision between the cart and the guy occurred
        GuyX : out std_logic_vector(9 downto 0); --X center location
        GuyY : out std_logic_vector(9 downto 0); --Y center location
		BallX : in std_logic_vector(9 downto 0); --X helicopter location
		BallY : in std_logic_vector(9 downto 0); --Y helicopter location
		space : out std_logic; -- for testing
		spaceone : out std_logic; --space hit once
		hground : out std_logic; -- 2 lives  -- these four signals hground and below are the four states used for calculating the lives
		hoground : out std_logic; -- 1 life
		hoground2 : out std_logic; -- 0 lives
		hoground3 : out std_logic); -- 3 lives
end guy;

architecture Behavioral of guy is

signal Ball_X_pos, Ball_X_motion, Ball_Y_pos, Ball_Y_motion, tempx : std_logic_vector(9 downto 0);
signal spaceonce, hitground, holdground, holdground2, holdground3 : std_logic;


constant Ball_X_Min    : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(0, 10);  --Leftmost point on the X axis
constant Ball_X_Max    : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(639, 10);  --Rightmost point on the X axis
constant Ball_Y_Min    : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(0, 10);   --Topmost point on the Y axis
constant Ball_Y_Max    : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(479, 10);  --Bottommost point on the Y axis
                              
constant Ball_X_Step   : std_logic_vector(9 downto 0) := "0000000010";  --Step size on the X axis
constant Ball_Y_Step   : std_logic_vector(9 downto 0) := "0000000010";  --Step size on the Y axis

begin


-------------------------------------------------



  Move_Guy: process(Reset, frame_clk, BallX, BallY, lrd, detect)
  begin
	if(Reset = '1') then   --Asynchronous Reset
		Ball_Y_Motion <= "0000000000";
		Ball_X_Motion <= "0000000000";
		Ball_Y_pos <= BallY + "0000011100"; --sets the guy correctly below the helicopter
		Ball_X_pos <= BallX; -- sets the Guy's X coordinate the same as the helicopter
		spaceonce <= '0'; --represents the state in which space is pressed once
		space <= '0';-- this signal is for testing
		hitground <= '0'; --these four are states for the lives
		holdground <= '0';
		holdground2 <= '0';
		holdground3 <= '1';
    elsif(rising_edge(frame_clk)) then--spaceonce is a state variable. 
		if(lrd = "001" AND spaceonce = '0') then --SPACE is pressed,for the first time, drop guy
			Ball_Y_motion <= "0000000011";
			space <= '1';
			Ball_X_motion <= "0000000000";
			spaceonce <= '1';
			Ball_X_pos <= BallX;
			tempx <= BallX;
			--Ball_X_pos <= tempx;
		elsif(lrd = "001" AND spaceonce = '1') then --SPACE is pressed for the second time, don't change the
			Ball_Y_motion <= "0000000011";			-- guys X location from before but let him continue dropping
			Ball_X_pos <= tempx;
			space <= '1';
			Ball_X_motion <= "0000000000";
			Ball_Y_pos <= Ball_Y_pos + Ball_Y_Motion;
		elsif(spaceonce = '1') then --continue dropping the guy
			Ball_Y_pos <= Ball_Y_pos + Ball_Y_Motion;
			space <= '0';
		else
			Ball_Y_pos <= BallY + "0000011100";  -- set the guy back underneath the helicopter because spaceonce = 0
			Ball_X_pos <= BallX;
		end if;
		
		--this giant if else statement is determining if and how many times the guy has hit the ground
		if((Ball_Y_Pos + "0000000110" >= Ball_Y_Max and hitground = '0' and holdground = '0')) then --hit the ground once, set lives to 2
			Ball_Y_pos <= BallY + "0000011100";
			Ball_X_pos <= BallX;
			Ball_Y_Motion <= "0000000000";
			Ball_X_Motion <= "0000000000";
			spaceonce <= '0';
			hitground <= '1';
			holdground <= '0';
			holdground2 <= '0';
			holdground3 <= '1';
		elsif((Ball_Y_Pos + "0000000110" >= Ball_Y_Max and hitground = '1' and holdground2 = '0')) then --hit ground twice, set lives to 1
			Ball_Y_pos <= BallY + "0000011100";
			Ball_X_pos <= BallX;
			Ball_Y_Motion <= "0000000000";
			Ball_X_Motion <= "0000000000";
			spaceonce <= '0';
			hitground <= '0';
			holdground <= '1';
			holdground2 <= '0';
			holdground3 <= '1';
		elsif((Ball_Y_Pos + "0000000110" >= Ball_Y_Max and holdground = '1')) then --hit ground 3 times, set lives to 0
			Ball_Y_pos <= BallY + "0000011100";
			Ball_X_pos <= BallX;
			Ball_Y_Motion <= "0000000000";
			Ball_X_Motion <= "0000000000";
			spaceonce <= '0';
			hitground <= '0';
			holdground <= '0';
			holdground2 <= '1';
			holdground3 <= '1';
		else 
			holdground3 <= '1'; --otherwise the lives are at 3
		end if;
		if(detect = '1') then --resets location of the guy if he successfully hits the cart
			Ball_Y_pos <= BallY + "0000011100";
			Ball_X_pos <= BallX;
			Ball_Y_Motion <= "0000000000";
			Ball_X_Motion <= "0000000000";
			spaceonce <= '0';

		end if;
    end if;
  end process Move_Guy;
  hground <= hitground; --set outputs
  hoground <= holdground;
  hoground2 <= holdground2;
  hoground3 <= holdground3;
  GuyX <= Ball_X_pos;
  GuyY <= Ball_Y_pos;
  spaceone <= spaceonce;

 
end Behavioral;  