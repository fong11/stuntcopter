library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity cartSpeed is
	port( 	clk : in std_logic;
			psclk : in std_logic;
			reset: in std_logic;
			speedsel : out std_logic_vector(2 downto 0));
end entity;

architecture behavioral of cartSpeed is
signal sel : std_logic_vector(2 downto 0);
signal clockchanger: std_logic;

begin


change_clock : process(psclk) --this process turns the data of the psclk into data so the count_now process will run
begin

if(reset = '1') then
	clockchanger <= '0';
elsif(rising_edge(psclk)) then
	clockchanger <= '1';
else
	clockchanger <= '0';
end if;

end process change_clock;




count_now : process(psclk, clk) --this process is a counter that relies on both the clk and psclk. 
begin 							-- the psclk was added just to add another factor of randomness
if(reset = '1') then
	clockchanger <= '0';
	sel <= "000";
elsif(clockchanger <= '1' OR rising_edge(clk)) then
	if(sel = "111") then
		sel <= "000";
	else
		sel <= sel + "001";
	end if;
	speedsel <= sel;
end if;

end process count_now;


end behavioral;
