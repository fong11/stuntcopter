-- Copyright (C) 1991-2010 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II"
-- VERSION		"Version 9.1 Build 350 03/24/2010 Service Pack 2 SJ Web Edition"
-- CREATED		"Tue Apr 30 16:29:53 2013"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY final_project IS 
	PORT
	(
		Clk :  IN  STD_LOGIC;
		Reset :  IN  STD_LOGIC;
		ps2Data :  IN  STD_LOGIC;
		ps2Clk :  IN  STD_LOGIC;
		VGA_clk :  OUT  STD_LOGIC;
		sync :  OUT  STD_LOGIC;
		blank :  OUT  STD_LOGIC;
		vs :  OUT  STD_LOGIC;
		hs :  OUT  STD_LOGIC;
		Collision :  OUT  STD_LOGIC;
		SpaceOnce :  OUT  STD_LOGIC;
		Right :  OUT  STD_LOGIC;
		Space :  OUT  STD_LOGIC;
		cart_right_edge :  OUT  STD_LOGIC;
		Blue :  OUT  STD_LOGIC_VECTOR(9 DOWNTO 0);
		Green :  OUT  STD_LOGIC_VECTOR(9 DOWNTO 0);
		Hex :  OUT  STD_LOGIC_VECTOR(6 DOWNTO 0);
		Hex1 :  OUT  STD_LOGIC_VECTOR(6 DOWNTO 0);
		Red :  OUT  STD_LOGIC_VECTOR(9 DOWNTO 0);
		Speed :  OUT  STD_LOGIC_VECTOR(2 DOWNTO 0);
		SpeedSel :  OUT  STD_LOGIC_VECTOR(2 DOWNTO 0)
	);
END final_project;

ARCHITECTURE bdf_type OF final_project IS 

COMPONENT keyboardvhdl
	PORT(CLK : IN STD_LOGIC;
		 RST : IN STD_LOGIC;
		 ps2Data : IN STD_LOGIC;
		 ps2Clk : IN STD_LOGIC;
		 NewKeyAck : IN STD_LOGIC;
		 newKeyOut : OUT STD_LOGIC;
		 keyCode : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;

COMPONENT keyboardcontroller
	PORT(Clk : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 enable : IN STD_LOGIC;
		 keycode : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 enter : OUT STD_LOGIC;
		 lrd : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
	);
END COMPONENT;

COMPONENT helicopter
	PORT(Reset : IN STD_LOGIC;
		 frame_clk : IN STD_LOGIC;
		 lrd : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		 up : OUT STD_LOGIC;
		 lft0rgt1 : OUT STD_LOGIC;
		 down : OUT STD_LOGIC;
		 BallX : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 BallY : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
	);
END COMPONENT;

COMPONENT cart
	PORT(Reset : IN STD_LOGIC;
		 frame_clk : IN STD_LOGIC;
		 speed : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		 test : OUT STD_LOGIC;
		 right_edge : OUT STD_LOGIC;
		 CartX : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 CartY : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
	);
END COMPONENT;

COMPONENT color_mapper
	PORT(lft0rgt1 : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 clk : IN STD_LOGIC;
		 Nter : IN STD_LOGIC;
		 hitground : IN STD_LOGIC;
		 holdground : IN STD_LOGIC;
		 holdground2 : IN STD_LOGIC;
		 holdground3 : IN STD_LOGIC;
		 BallX : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 BallY : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 CartX : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 CartY : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 DrawX : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 DrawY : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 DudeX : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 DudeY : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 Collision : OUT STD_LOGIC;
		 enable : OUT STD_LOGIC;
		 guyreset : OUT STD_LOGIC;
		 Blue : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 Green : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 Red : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 Scr1 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		 Scr10 : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;

COMPONENT vga_controller
	PORT(clk : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 hs : OUT STD_LOGIC;
		 vs : OUT STD_LOGIC;
		 pixel_clk : OUT STD_LOGIC;
		 blank : OUT STD_LOGIC;
		 sync : OUT STD_LOGIC;
		 DrawX : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 DrawY : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
	);
END COMPONENT;

COMPONENT guy
	PORT(Reset : IN STD_LOGIC;
		 frame_clk : IN STD_LOGIC;
		 detect : IN STD_LOGIC;
		 BallX : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 BallY : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 lrd : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		 space : OUT STD_LOGIC;
		 spaceone : OUT STD_LOGIC;
		 hground : OUT STD_LOGIC;
		 hoground : OUT STD_LOGIC;
		 hoground2 : OUT STD_LOGIC;
		 hoground3 : OUT STD_LOGIC;
		 GuyX : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 GuyY : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
	);
END COMPONENT;

COMPONENT hexdriver
	PORT(In0 : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
		 Out0 : OUT STD_LOGIC_VECTOR(6 DOWNTO 0)
	);
END COMPONENT;

COMPONENT cartspeed
	PORT(clk : IN STD_LOGIC;
		 psclk : IN STD_LOGIC;
		 reset : IN STD_LOGIC;
		 speedsel : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
	);
END COMPONENT;

COMPONENT speedmux2
	PORT(Sel : IN STD_LOGIC_VECTOR(2 DOWNTO 0);
		 Speed : OUT STD_LOGIC_VECTOR(2 DOWNTO 0)
	);
END COMPONENT;

SIGNAL	C :  STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL	D :  STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL	Out :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	Output :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_35 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_1 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_3 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_4 :  STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_36 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_37 :  STD_LOGIC_VECTOR(2 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_10 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_12 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_13 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_14 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_15 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_16 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_38 :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_39 :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_19 :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_20 :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_21 :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_22 :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_23 :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_24 :  STD_LOGIC_VECTOR(9 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_26 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_28 :  STD_LOGIC;
SIGNAL	SYNTHESIZED_WIRE_34 :  STD_LOGIC;


BEGIN 
vs <= SYNTHESIZED_WIRE_36;
Collision <= SYNTHESIZED_WIRE_28;
Right <= SYNTHESIZED_WIRE_10;
SYNTHESIZED_WIRE_1 <= '0';



b2v_inst : keyboardvhdl
PORT MAP(CLK => Clk,
		 RST => SYNTHESIZED_WIRE_35,
		 ps2Data => ps2Data,
		 ps2Clk => ps2Clk,
		 NewKeyAck => SYNTHESIZED_WIRE_1,
		 keyCode => SYNTHESIZED_WIRE_4);


b2v_inst1 : keyboardcontroller
PORT MAP(Clk => Clk,
		 reset => SYNTHESIZED_WIRE_35,
		 enable => SYNTHESIZED_WIRE_3,
		 keycode => SYNTHESIZED_WIRE_4,
		 enter => SYNTHESIZED_WIRE_12,
		 lrd => SYNTHESIZED_WIRE_37);


b2v_inst10 : helicopter
PORT MAP(Reset => SYNTHESIZED_WIRE_35,
		 frame_clk => SYNTHESIZED_WIRE_36,
		 lrd => SYNTHESIZED_WIRE_37,
		 lft0rgt1 => SYNTHESIZED_WIRE_10,
		 BallX => SYNTHESIZED_WIRE_38,
		 BallY => SYNTHESIZED_WIRE_39);



b2v_inst2 : cart
PORT MAP(Reset => SYNTHESIZED_WIRE_35,
		 frame_clk => SYNTHESIZED_WIRE_36,
		 speed => C,
		 test => SpaceOnce,
		 right_edge => cart_right_edge,
		 CartX => SYNTHESIZED_WIRE_19,
		 CartY => SYNTHESIZED_WIRE_20);


b2v_inst20 : color_mapper
PORT MAP(lft0rgt1 => SYNTHESIZED_WIRE_10,
		 reset => SYNTHESIZED_WIRE_35,
		 clk => Clk,
		 Nter => SYNTHESIZED_WIRE_12,
		 hitground => SYNTHESIZED_WIRE_13,
		 holdground => SYNTHESIZED_WIRE_14,
		 holdground2 => SYNTHESIZED_WIRE_15,
		 holdground3 => SYNTHESIZED_WIRE_16,
		 BallX => SYNTHESIZED_WIRE_38,
		 BallY => SYNTHESIZED_WIRE_39,
		 CartX => SYNTHESIZED_WIRE_19,
		 CartY => SYNTHESIZED_WIRE_20,
		 DrawX => SYNTHESIZED_WIRE_21,
		 DrawY => SYNTHESIZED_WIRE_22,
		 DudeX => SYNTHESIZED_WIRE_23,
		 DudeY => SYNTHESIZED_WIRE_24,
		 Collision => SYNTHESIZED_WIRE_28,
		 enable => SYNTHESIZED_WIRE_3,
		 guyreset => SYNTHESIZED_WIRE_34,
		 Blue => Blue,
		 Green => Green,
		 Red => Red,
		 Scr1 => Output,
		 Scr10 => Out);


b2v_inst21 : vga_controller
PORT MAP(clk => Clk,
		 reset => SYNTHESIZED_WIRE_35,
		 hs => hs,
		 vs => SYNTHESIZED_WIRE_36,
		 pixel_clk => VGA_clk,
		 blank => blank,
		 sync => sync,
		 DrawX => SYNTHESIZED_WIRE_21,
		 DrawY => SYNTHESIZED_WIRE_22);


b2v_inst3 : guy
PORT MAP(Reset => SYNTHESIZED_WIRE_26,
		 frame_clk => SYNTHESIZED_WIRE_36,
		 detect => SYNTHESIZED_WIRE_28,
		 BallX => SYNTHESIZED_WIRE_38,
		 BallY => SYNTHESIZED_WIRE_39,
		 lrd => SYNTHESIZED_WIRE_37,
		 space => Space,
		 hground => SYNTHESIZED_WIRE_13,
		 hoground => SYNTHESIZED_WIRE_14,
		 hoground2 => SYNTHESIZED_WIRE_15,
		 hoground3 => SYNTHESIZED_WIRE_16,
		 GuyX => SYNTHESIZED_WIRE_23,
		 GuyY => SYNTHESIZED_WIRE_24);


b2v_inst4 : hexdriver
PORT MAP(In0 => Output(3 DOWNTO 0),
		 Out0 => Hex);


b2v_inst5 : hexdriver
PORT MAP(In0 => Out(3 DOWNTO 0),
		 Out0 => Hex1);


SYNTHESIZED_WIRE_35 <= NOT(Reset);



b2v_inst7 : cartspeed
PORT MAP(clk => Clk,
		 psclk => ps2Clk,
		 reset => SYNTHESIZED_WIRE_35,
		 speedsel => D);


b2v_inst8 : speedmux2
PORT MAP(Sel => D,
		 Speed => C);


SYNTHESIZED_WIRE_26 <= SYNTHESIZED_WIRE_35 OR SYNTHESIZED_WIRE_34;

Speed <= C;
SpeedSel <= D;

END bdf_type;