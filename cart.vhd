library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- This entity represents the cart

entity cart is
   Port ( Reset : in std_logic;
        frame_clk : in std_logic;
        CartX : out std_logic_vector(9 downto 0);
        CartY : out std_logic_vector(9 downto 0);
		test : out std_logic;
		speed : in std_logic_vector(2 downto 0);
		right_edge : out std_logic);
end cart;

architecture Behavioral of cart is

signal Ball_X_pos, Ball_X_motion, Ball_Y_pos, Ball_Y_motion : std_logic_vector(9 downto 0);
signal testsig : std_logic;


constant Ball_X_Center : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(1, 10);  --Center position on the X axis
constant Ball_Y_Center : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(350, 10);  --435 Center position on the Y axis

constant Ball_X_Min    : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(0, 10);  --Leftmost point on the X axis
constant Ball_X_Max    : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(639, 10);  --Rightmost point on the X axis
constant Ball_Y_Min    : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(0, 10);   --Topmost point on the Y axis
constant Ball_Y_Max    : std_logic_vector(9 downto 0) := CONV_STD_LOGIC_VECTOR(479, 10);  --Bottommost point on the Y axis
                              
constant Ball_X_Step   : std_logic_vector(9 downto 0) := "0000000100";  --Step size on the X axis
constant Ball_Y_Step   : std_logic_vector(9 downto 0) := "0000000010";  --Step size on the Y axis

begin
 

-------------------------------------------------

-- testsig and right_edge were just used for testing purposes

  Move_Cart: process(Reset, frame_clk, Ball_X_pos)
  begin
	if(Reset = '1') then   --Asynchronous Reset
		Ball_Y_Motion <= "0000000000";
		testsig <= '0';
		right_edge <= '0';

		if(Ball_X_Motion <= "0000000000") then --if speedmux doesnt output anything, we automatically set the motion to 2, otherwise we set it according to the speed. 
			Ball_X_Motion <= "0000000010";
			testsig <= '1';
		else
			Ball_X_Motion <= "0000000"&speed;
		end if;
		
		Ball_Y_pos <= Ball_Y_Center; --set the cart to the bottom left part of the screen
		Ball_X_pos <= Ball_X_Center; -- Ball_X and Ball_Y center in this case represents that position
		--testsig <= '0'
    elsif(rising_edge(frame_clk)) then
		if(Ball_X_pos >= Ball_X_Max + "0000010111") then -- Ball is at the right edge, RESET position
			Ball_X_pos <= "0000000001";
			right_edge <= '1';
		
		if(Ball_X_Motion <= "0000000000") then -- same conition as above. We want to set the motion to 2 if speed = 0;
			Ball_X_Motion <= "0000000010";
		else
			Ball_X_Motion <= "0000000"&speed;
			testsig <= '1';
		end if;

		else
		if(Ball_X_Motion <= "0000000000") then -- same
			Ball_X_Motion <= "0000000010";
		end if;
			Ball_X_pos <= Ball_X_pos + Ball_X_Motion;
			right_edge <= '0';
		end if;
    end if;
  end process Move_Cart;

  test <= testsig;
  CartX <= Ball_X_pos; -- these set the outputs that go to the Color Mapper entity
  CartY <= Ball_Y_pos; -- represent the center position of the cart
 
end Behavioral;  